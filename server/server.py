from worker import add


def application(environ, start_response):
    result = add.delay(2, 2)
    """Simplest possible application object"""
    data = bytes(str(result.get()), 'utf-8')
    status = '200 OK'
    response_headers = [
        ('Content-type', 'text/plain'),
        ('Content-Length', str(len(data)))
    ]
    start_response(status, response_headers)
    return iter([data])